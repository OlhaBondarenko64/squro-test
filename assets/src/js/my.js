
$(function () {
    /* Value counter */
    var $valueTotal = $('.valueTotal');

    var $valueOneText = $('.valueOneText');
    var $valueOneSlide = $('#valueOneSlide');

    var $valueTwoText = $('.valueTwoText');
    var $valueTwoSlide = $('#valueTwoSlide');
    
    $valueOneText.html($valueOneSlide.val());
    $valueTwoText.html($valueTwoSlide.val());

    $num1 = Number($valueOneSlide.val());
    $num2 = Number($valueTwoSlide.val());
    var $total = $num1 * $num2;
    
    $valueTotal.html($total + " Points");

    function calcTotal($v1, $v2){
        $num1 = Number($v1.val());
        $num2 = Number($v2.val());
        $total = $num1 * $num2;
        $valueTotal.html($total + " Points");
    }

    $valueOneSlide.on('input change', function() {  
        $valueOneText.html($valueOneSlide.val());
        calcTotal($valueOneSlide, $valueTwoSlide);
    });
    $valueTwoSlide.on('input change', function() {  
        $valueTwoText.html($valueTwoSlide.val());
        calcTotal($valueOneSlide, $valueTwoSlide);
    });

    $( '.slider input' ).on( 'input', function( ) {
        $( this ).css( 'background', 'linear-gradient(to right, green 0%, green '+this.value +'%, #fff ' + this.value + '%, white 100%)' );
      } );

    /* show list languages */
    $('#langList li').click(function(){
        $('#lang').html($(this).text() + '<span class="caret"></span>')
    });

});

/* hide menu if press escape button */
var menuToggle = document.getElementById("menu-toggle");
$(document).on('keyup',function(evt) {
    
    if (evt.keyCode == 27 && menuToggle.checked == true) {
        menuToggle.checked = false;
    }
});

